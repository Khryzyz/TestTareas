<?php

	use Illuminate\Database\Seeder;
	use Tareas\Globals\Parametricas;
	use Tareas\Models\User;
	use Spatie\Permission\Models\Role;
	use Spatie\Permission\Models\Permission;

	class InicialSeeder extends Seeder {

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {

			/**
			 * Usuario administrador
			 */
			$user = User::create([
				'email'        => 'chris3154@gmail.com',
				'password'     => '1121843962',
				'nombre'       => 'Christhian Hernando',
				'apellido'     => 'Torres Niño',
				'tipo_user_id' => Parametricas::TIPO_USER_DEVELOPER,
			]);

			$role = Role::create([
				'name' => 'developer',
			]);

			$permission = Permission::create([
				'name' => 'permission.create',
			]);

			$role->givePermissionTo($permission);

			$user->assignRole('developer');

		}
	}
