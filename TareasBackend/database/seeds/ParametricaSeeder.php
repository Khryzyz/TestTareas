<?php

	use Illuminate\Database\Seeder;
	use Tareas\Globals\Parametricas;
	use Tareas\Models\TipoTarea;
	use Tareas\Models\TipoUser;

	class ParametricaSeeder extends Seeder {

		/**
		 * Run the database seeds.
		 *
		 * @return void
		 */
		public function run() {

			/**
			 * Tipos Tareas
			 */

			TipoTarea::create([
				'id'     => Parametricas::TIPO_TAREA_EXAMEN,
				'nombre' => "Examen",
			]);

			TipoTarea::create([
				'id'     => Parametricas::TIPO_TAREA_PRESENTACION,
				'nombre' => "Presentacion",
			]);

			TipoTarea::create([
				'id'     => Parametricas::TIPO_TAREA_HOGAR,
				'nombre' => "Hogar",
			]);

			/**
			 * Tipos User
			 */

			TipoUser::create([
				'id'     => Parametricas::TIPO_USER_DEVELOPER,
				'sigla'  => "D",
				'nombre' => "Developer",
			]);

			TipoUser::create([
				'id'     => Parametricas::TIPO_USER_ADMINISTRADOR,
				'sigla'  => "A",
				'nombre' => "Presentacion",
			]);

			TipoUser::create([
				'id'     => Parametricas::TIPO_USER_REGULAR,
				'sigla'  => "R",
				'nombre' => "Hogar",
			]);

		}
	}
