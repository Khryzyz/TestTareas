<?php

	use Tareas\Globals\Parametricas;
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateUsersTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {

			Schema::dropIfExists('users');

			Schema::create('users', function (Blueprint $table) {

				$table->increments('id');
				$table->string('email')->unique();
				$table->string('password');
				$table->string('nombre');
				$table->string('apellido')->nullable();
				$table->unsignedInteger('tipo_user_id')->default(Parametricas::TIPO_USER_REGULAR);
				$table->rememberToken();
				$table->timestamps();
				$table->softDeletes();
				//Creacion de la llave foranea a la tabla estado
				$table->foreign('tipo_user_id')
					  ->references('id')->on('tipo_users')
					  ->onDelete('cascade');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {

			Schema::dropIfExists('users');
		}
	}
