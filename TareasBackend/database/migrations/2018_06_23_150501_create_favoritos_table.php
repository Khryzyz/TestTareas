<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateFavoritosTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {

			Schema::dropIfExists('favoritos');

			Schema::create('favoritos', function (Blueprint $table) {

				$table->increments('id');
				$table->unsignedInteger('tarea_id');
				$table->unsignedInteger('user_id');
				$table->timestamps();
				$table->softDeletes();
				//Creacion de la llave foranea a la tabla tareas
				$table->foreign('tarea_id')
					  ->references('id')->on('tareas')
					  ->onDelete('cascade');
				//Creacion de la llave foranea a la tabla user
				$table->foreign('user_id')
					  ->references('id')->on('users')
					  ->onDelete('cascade');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {

			Schema::dropIfExists('favoritos');
		}
	}
