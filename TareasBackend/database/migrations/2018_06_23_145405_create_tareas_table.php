<?php

	use Tareas\Globals\Parametricas;
	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateTareasTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {

			Schema::dropIfExists('tareas');

			Schema::create('tareas', function (Blueprint $table) {

				$table->increments('id');
				$table->string('nombre');
				$table->unsignedInteger('tipo_tarea_id')->default(Parametricas::TIPO_TAREA_PRESENTACION);
				$table->unsignedInteger('user_id');
				$table->timestamps();
				$table->softDeletes();
				//Creacion de la llave foranea a la tabla tipo_tareas
				$table->foreign('tipo_tarea_id')
					  ->references('id')->on('tipo_tareas')
					  ->onDelete('cascade');
				//Creacion de la llave foranea a la tabla user
				$table->foreign('user_id')
					  ->references('id')->on('users')
					  ->onDelete('cascade');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {

			Schema::dropIfExists('tareas');
		}
	}
