<?php

	use Illuminate\Database\Migrations\Migration;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Support\Facades\Schema;

	class CreateCursoUsersTable extends Migration {

		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up() {

			Schema::dropIfExists('curso_users');

			Schema::create('curso_users', function (Blueprint $table) {

				$table->unsignedInteger('curso_id');
				$table->unsignedInteger('user_id');
				//Creacion de la llave foranea a la tabla cursos
				$table->foreign('curso_id')
					  ->references('id')->on('cursos')
					  ->onDelete('cascade');
				//Creacion de la llave foranea a la tabla users
				$table->foreign('user_id')
					  ->references('id')->on('users')
					  ->onDelete('cascade');
			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down() {

			Schema::dropIfExists('curso_users');
		}
	}
