<?php

	use Illuminate\Http\Request;

	/*
	|--------------------------------------------------------------------------
	| API Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register API routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| is assigned the "api" middleware group. Enjoy building your API!
	|
	*/
	/**
	 * Rutas publicas
	 */
	Route::post('login', 'AuthController@login');
	/**
	 * Rutas protegidas por el JWT
	 */
	Route::group(['middleware' => 'jwt-auth'], function () {

		/**
		 * Mi información
		 */
		Route::post('me', 'AuthController@me');
		/**
		 * Refresco del token
		 */
		Route::post('refresh', 'AuthController@refresh');
		/**
		 * Logout del sistema
		 */
		Route::post('logout', 'AuthController@logout');
		/**
		 *  Rutas al recurso User
		 */
		Route::resource('user', 'UserController')->except(['create', 'edite',]);
		/**
		 * Ruta para hacer flush a la cache del User
		 */
		Route::get('flushCacheUser', 'UserController@flushCacheUser');
		/**
		 * Rutas al recurso Tarea
		 */
		Route::resource('tarea', 'TareaController', ['except' => ['create', 'edit']]);
		/**
		 * Rutas al recurso Curso
		 */
		Route::resource('curso', 'CursoController', ['except' => ['create', 'edit']]);
	});



