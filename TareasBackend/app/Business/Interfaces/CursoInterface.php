<?php

	namespace Tareas\Business\Interfaces;

	interface CursoInterface {

		/**
		 * Display a listing of the resource.
		 */
		public function index();

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 */
		public function show($id);

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 */
		public function store($request);

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 */
		public function update($request, $id);

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 */
		public function destroy($id);
	}
