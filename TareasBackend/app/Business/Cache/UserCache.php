<?php

	namespace Tareas\Business\Repositories;

	use Illuminate\Support\Facades\Cache;
	use Tareas\Business\Interfaces\UserInterface;
	use Tareas\Globals\Parametricas;

	class UserCache implements UserInterface {

		protected $userRepository;

		protected $tag = "user";

		/**
		 * UserCache constructor.
		 *
		 * @param \Tareas\Business\Repositories\UserRepository $userRepository
		 */
		public function __construct(UserRepository $userRepository) {

			$this->userRepository = $userRepository;
		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			$key = $this->tag.".index";

			return Cache::tags($this->tag)->remember($key, Parametricas::CACHE_TIME, function () {
				return $this->userRepository->index();
			});

		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			$key = $this->tag.".show.".$id;

			return Cache::tags($this->tag)->rememberForever($key, function () use ($id) {
				return $this->userRepository->show($id);
			});

		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 *
		 * @return mixed
		 */
		public function store($request) {

			Cache::tags($this->tag)->flush();

			$dataModel = $this->userRepository->store($request);

			return $dataModel;

		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function update($request, $id) {

			Cache::tags($this->tag)->flush();

			$dataModel = $this->userRepository->update($request, $id);

			return $dataModel;
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function destroy($id) {

			Cache::tags($this->tag)->flush();

			$dataModel = $this->userRepository->destroy($id);

			return $dataModel;

		}

		/**
		 * Flush cache
		 *
		 * @return mixed
		 */
		public function flushCacheUser() {

			return Cache::tags($this->tag)->flush();

		}
	}
