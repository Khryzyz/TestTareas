<?php

	namespace Tareas\Business\Repositories;

	use Tareas\Business\Interfaces\FavoritoInterface;
	use Tareas\Models\Favorito;

	class FavoritoRepository implements FavoritoInterface {

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			return Favorito::all();
		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			return Favorito::findorfail($id);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 *
		 * @return mixed
		 */
		public function store($request) {

			return Favorito::create($request->all());
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function update($request, $id) {

			try {
				return Favorito::findorfail($id)->update($request->all());
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function destroy($id) {

			try {
				return Favorito::findorfail($id)->delete();
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}
	}
