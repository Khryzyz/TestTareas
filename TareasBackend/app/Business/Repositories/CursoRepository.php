<?php

	namespace Tareas\Business\Repositories;

	use Tareas\Business\Interfaces\CursoInterface;
	use Tareas\Models\Curso;

	class CursoRepository implements CursoInterface {

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			return Curso::all();
		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			return Curso::findorfail($id);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 *
		 * @return mixed
		 */
		public function store($request) {

			return Curso::create($request->all());
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function update($request, $id) {

			try {
				return Curso::findorfail($id)->update($request->all());
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function destroy($id) {

			try {
				return Curso::findorfail($id)->delete();
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}
	}
