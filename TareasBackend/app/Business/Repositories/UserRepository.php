<?php

	namespace Tareas\Business\Repositories;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Tareas\Business\Interfaces\UserInterface;
	use Tareas\Models\User;

	class UserRepository implements UserInterface {

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Database\Eloquent\Collection|static[]
		 */
		public function index() {

			return User::all();
		}

		/**
		 * Display the specified resource.
		 *
		 * @param $id
		 *
		 * @return mixed
		 */
		public function show($id) {

			return User::findorfail($id);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param $request
		 *
		 * @return mixed
		 */
		public function store($request) {

			return User::create($request->all());
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param $request
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function update($request, $id) {

			try {
				return User::findorfail($id)->update($request->all());
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param $id
		 *
		 * @return null|mixed
		 */
		public function destroy($id) {

			try {
				return User::findorfail($id)->delete();
			} catch (ModelNotFoundException $err) {
				return null;
			}
		}
	}
