<?php

	namespace Tareas\Http\Middleware;

	use Closure;
	use Tareas\Globals\CodesResponse;
	use Tareas\Globals\KeysResponse;
	use Tareas\Globals\MessageResponse;
	use Tymon\JWTAuth\Exceptions\JWTException;
	use Tymon\JWTAuth\Exceptions\TokenExpiredException;
	use Tymon\JWTAuth\JWTAuth;

	class authJWT {

		/**
		 * The JWT Authenticator.
		 *
		 * @var \Tymon\JWTAuth\JWTAuth
		 */
		protected $auth;

		/**
		 * Create a new BaseMiddleware instance.
		 *
		 * @param  \Tymon\JWTAuth\JWTAuth $auth
		 *
		 * @return void
		 */
		public function __construct(JWTAuth $auth) {

			$this->auth = $auth;
		}

		/**
		 * Handle an incoming request.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  \Closure                 $next
		 *
		 * @return mixed
		 */
		public function handle($request, Closure $next) {

			$error           = false;
			$responseStatus  = null;
			$responseMessage = null;
			$responseCode    = null;
			try {
				if (!$this->auth->parser()->setRequest($request)->hasToken()) {
					$error           = true;
					$responseStatus  = KeysResponse::STATUS_ERROR;
					$responseMessage = MessageResponse::MESSAGE_AUTH_TOKEN_NOT_FOUND;
					$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
				}
				if (!$this->auth->parseToken()->authenticate()) {
					$error           = true;
					$responseStatus  = KeysResponse::STATUS_ERROR;
					$responseMessage = MessageResponse::MESSAGE_AUTH_USER_NOT_FOUND;
					$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
				}
			} catch (TokenExpiredException $exception) {
				$error           = true;
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_AUTH_TOKEN_EXPIRED;
				$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
			} catch (JWTException $e) {
				$error           = true;
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_AUTH_TOKEN_INVALID;
				$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
			}
			if ($error) {
				return response()->json(
					[
						KeysResponse::KEY_STATUS  => $responseStatus,
						KeysResponse::KEY_MESSAGE => $responseMessage,
						KeysResponse::KEY_DATA    => null,
					],
					$responseCode
				);
			}

			return $next($request);
		}
	}
