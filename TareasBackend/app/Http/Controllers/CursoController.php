<?php

	namespace Tareas\Http\Controllers;

	use Illuminate\Support\Facades\DB;
	use Tareas\Http\Requests\CursoRequest;
	use Tareas\Business\Repositories\CursoRepository;
	use Tareas\Globals\KeysResponse;
	use Tareas\Globals\MethodsHttp;
	use Tareas\Globals\Utils;

	class CursoController extends Controller {

		protected $repository;

		/**
		 * TareaController constructor.
		 *
		 * @param $repository
		 */
		public function __construct(CursoRepository $repository) {

			$this->repository = $repository;
		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index() {

			try {
				$dataModel = $this->repository->index();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function show($id) {

			try {
				$dataModel = $this->repository->show($id);

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param \Tareas\Http\Requests\CursoRequest $dataRequest
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function store(CursoRequest $dataRequest) {

			try {
				DB::beginTransaction();
				$dataModel = $this->repository->store($dataRequest);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_POST
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_POST
				);
			}
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param \Tareas\Http\Requests\CursoRequest $request
		 * @param                                    $id
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function update(CursoRequest $request, $id) {

			try {
				DB::beginTransaction();
				$dataModel = $this->repository->update($request, $id);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_PUT
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_PUT
				);
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id) {

			try {
				DB::beginTransaction();
				$dataModel = $this->repository->destroy($id);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_DELETE
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_DELETE
				);
			}
		}
	}
