<?php

	namespace Tareas\Http\Controllers;

	use Illuminate\Support\Facades\DB;
	use Tareas\Http\Requests\TareaRequest;
	use Tareas\Business\Repositories\TareaRepository;
	use Tareas\Globals\KeysResponse;
	use Tareas\Globals\MethodsHttp;
	use Tareas\Globals\Utils;

	class TareaController extends Controller {

		protected $repository;

		protected $user;

		/**
		 * TareaController constructor.
		 *
		 * @param $repository
		 */
		public function __construct(TareaRepository $repository) {

			$this->repository = $repository;

			$this->user = auth()->user();
		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index() {

			// $this->user->hasPermissionTo('tarea.editar');

			try {
				$dataModel = $this->repository->index();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function show($id) {

			try {
				$dataModel = $this->repository->show($id);

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param \Tareas\Http\Requests\TareaRequest $request
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function store(TareaRequest $request) {

			try {
				DB::beginTransaction();
				$dataModel = $this->repository->store($request);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_POST
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_POST
				);
			}
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param \Tareas\Http\Requests\TareaRequest $tareaRequest
		 * @param                                    $id
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function update(TareaRequest $tareaRequest, $id) {

			try {
				DB::beginTransaction();
				$dataModel = $this->repository->update($tareaRequest, $id);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_PUT
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_PUT
				);
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id) {

			try {
				DB::beginTransaction();
				$dataModel = $this->repository->destroy($id);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_DELETE
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_DELETE
				);
			}
		}
	}
