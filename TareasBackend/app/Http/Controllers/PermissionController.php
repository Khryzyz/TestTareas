<?php

	namespace Tareas\Http\Controllers;

	use Illuminate\Http\Request;

	class PermissionController extends Controller {

		/**
		 * Flush cache User
		 */
		public function flushCacheUser() {

			$dataModel = $this->userCache->flushCacheUser();

			return response()->json(
				[
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CACHE_FLUSH,
					KeysResponse::KEY_DATA    => $dataModel,
				],
				CodesResponse::CODE_OK
			);
		}
	}
