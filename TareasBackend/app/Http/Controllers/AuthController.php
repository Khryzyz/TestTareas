<?php

	namespace Tareas\Http\Controllers;

	use Tareas\Models\User;
	use Illuminate\Http\Request;
	use Tareas\Globals\CodesResponse;
	use Tareas\Globals\KeysResponse;
	use Tareas\Globals\MessageResponse;
	use Illuminate\Support\Facades\Auth;
	use Tymon\JWTAuth\Exceptions\UserNotDefinedException;

	class AuthController extends Controller {

		protected $user;

		/**
		 * Create a new AuthController instance.
		 *
		 * @return void
		 */
		public function __construct() {

			$this->user = new User;
		}

		/**
		 * Get a JWT token via given credentials.
		 *
		 * @param  \Illuminate\Http\Request $request
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function login(Request $request) {

			$credentials     = $request->only('email', 'password');
			$token           = null;
			$responseStatus  = null;
			$responseMessage = null;
			$responseData    = null;
			$responseCode    = null;
			try {
				if ($token = \JWTAuth::attempt($credentials)) {
					$responseStatus  = KeysResponse::STATUS_SUCCESS;
					$responseMessage = MessageResponse::MESSAGE_LOGIN_SUCCESS;
					$responseData    = [
						"token"      => $token,
						'token_type' => 'bearer',
						'expires_in' => auth('api')->factory()->getTTL() * 60,
					];
					$responseCode    = CodesResponse::CODE_OK;
				}
				else {
					$responseStatus  = KeysResponse::STATUS_ERROR;
					$responseMessage = MessageResponse::MESSAGE_LOGIN_FAIL;
					$responseCode    = CodesResponse::CODE_BAD_REQUEST;
				}
			} catch (\JWTAuthException $e) {
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_LOGIN_ERROR;
				$responseCode    = CodesResponse::CODE_INTERNAL_SERVER;
			}

			return response()->json(
				[
					KeysResponse::KEY_STATUS  => $responseStatus,
					KeysResponse::KEY_MESSAGE => $responseMessage,
					KeysResponse::KEY_DATA    => $responseData,
				],
				$responseCode
			);
		}

		/**
		 * Get the authenticated User
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function me() {

			$responseStatus  = null;
			$responseMessage = null;
			$responseData    = null;
			$responseCode    = null;
			try {
				$user            = \JWTAuth::user();
				$responseStatus  = KeysResponse::STATUS_SUCCESS;
				$responseMessage = MessageResponse::MESSAGE_AUTH_USER_FOUND;
				$responseData    = $user;
				$responseCode    = CodesResponse::CODE_OK;
			} catch (UserNotDefinedException $e) {
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_AUTH_USER_ERROR;
				$responseCode    = CodesResponse::CODE_INTERNAL_SERVER;
			}

			return response()->json(
				[
					KeysResponse::KEY_STATUS  => $responseStatus,
					KeysResponse::KEY_MESSAGE => $responseMessage,
					KeysResponse::KEY_DATA    => $responseData,
				],
				$responseCode
			);
		}

		/**
		 * Log the user out (Invalidate the token)
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function logout() {

			$responseStatus  = null;
			$responseMessage = null;
			$responseCode    = null;
			try {
				Auth::logout();
				$responseStatus  = KeysResponse::STATUS_SUCCESS;
				$responseMessage = MessageResponse::MESSAGE_LOGOUT_SUCCESS;
				$responseCode    = CodesResponse::CODE_OK;
			} catch (\JWTAuthException $e) {
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_LOGOUT_ERROR;
				$responseCode    = CodesResponse::CODE_INTERNAL_SERVER;
			}

			return response()->json(
				[
					KeysResponse::KEY_STATUS  => $responseStatus,
					KeysResponse::KEY_MESSAGE => $responseMessage,
					KeysResponse::KEY_DATA    => null,
				],
				$responseCode
			);
		}

		/**
		 * Refresh a token.
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function refresh() {

			$token           = null;
			$responseStatus  = null;
			$responseMessage = null;
			$responseData    = null;
			$responseCode    = null;
			try {
				if ($token = \JWTAuth::refresh()) {
					$responseStatus  = KeysResponse::STATUS_SUCCESS;
					$responseMessage = MessageResponse::MESSAGE_REFRESH_SUCCESS;
					$responseData    = [
						"token"      => $token,
						'token_type' => 'bearer',
						'expires_in' => auth('api')->factory()->getTTL() * 60,
					];
					$responseCode    = CodesResponse::CODE_OK;
				}
				else {
					$responseStatus  = KeysResponse::STATUS_ERROR;
					$responseMessage = MessageResponse::MESSAGE_REFRESH_ERROR;
					$responseCode    = CodesResponse::CODE_BAD_REQUEST;
				}
			} catch (\JWTAuthException $e) {
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_REFRESH_ERROR;
				$responseCode    = CodesResponse::CODE_INTERNAL_SERVER;
			}

			return response()->json(
				[
					KeysResponse::KEY_STATUS  => $responseStatus,
					KeysResponse::KEY_MESSAGE => $responseMessage,
					KeysResponse::KEY_DATA    => $responseData,
				],
				$responseCode
			);
		}
	}
