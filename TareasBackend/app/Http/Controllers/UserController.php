<?php

	namespace Tareas\Http\Controllers;

	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\DB;
	use Tareas\Business\Repositories\UserCache;
	use Tareas\Globals\CodesResponse;
	use Tareas\Globals\KeysResponse;
	use Tareas\Globals\MessageResponse;
	use Tareas\Globals\MethodsHttp;
	use Tareas\Globals\Utils;
	use Tareas\Http\Requests\CursoRequest;

	class UserController extends Controller {

		protected $userCache;

		/**
		 * TareaController constructor.
		 *
		 * @param $userCache
		 */
		public function __construct(UserCache $userCache) {

			$this->userCache = $userCache;
		}

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index() {

			try {
				$dataModel = $this->userCache->index();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (ModelNotFoundException $e) {

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {
				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function show($id) {

			try {
				$dataModel = $this->userCache->show($id);

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (ModelNotFoundException $e) {

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_GET
				);
			} catch (\Exception $e) {

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param \Tareas\Http\Requests\CursoRequest $dataRequest
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function store(CursoRequest $dataRequest) {

			try {
				DB::beginTransaction();
				$dataModel = $this->userCache->store($dataRequest);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_POST
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_POST
				);
			}
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param \Tareas\Http\Requests\CursoRequest $request
		 * @param                                    $id
		 *
		 * @return \Illuminate\Http\JsonResponse
		 */
		public function update(CursoRequest $request, $id) {

			try {
				DB::beginTransaction();
				$dataModel = $this->userCache->update($request, $id);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_PUT
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_PUT
				);
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id) {

			try {
				DB::beginTransaction();
				$dataModel = $this->userCache->destroy($id);
				DB::commit();

				return Utils::responseTransaccion(
					$dataModel,
					KeysResponse::STATUS_SUCCESS,
					MethodsHttp::METHOD_DELETE
				);
			} catch (\Exception $e) {
				DB::rollBack();

				return Utils::responseTransaccion(
					null,
					KeysResponse::STATUS_ERROR,
					MethodsHttp::METHOD_DELETE
				);
			}
		}

		/**
		 * Flush cache User
		 */
		public function flushCacheUser() {

			$dataModel = $this->userCache->flushCacheUser();

			return response()->json(
				[
					KeysResponse::KEY_STATUS  => KeysResponse::STATUS_SUCCESS,
					KeysResponse::KEY_MESSAGE => MessageResponse::MESSAGE_CACHE_FLUSH,
					KeysResponse::KEY_DATA    => $dataModel,
				],
				CodesResponse::CODE_OK
			);
		}

	}
