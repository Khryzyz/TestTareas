<?php

	namespace Tareas\Http\Requests;

	use Illuminate\Contracts\Validation\Validator;
	use Illuminate\Foundation\Http\FormRequest;
	use Illuminate\Http\Exceptions\HttpResponseException;
	use Tareas\Globals\CodesResponse;
	use Tareas\Globals\MethodsHttp;

	class TareaRequest extends FormRequest {

		/**
		 * Determine if the user is authorized to make this request.
		 *
		 * @return bool
		 */
		public function authorize() {

			return true;
		}

		/**
		 * Funcion que maneja las reglas de validacion
		 *
		 * @return array
		 */
		public function rules() {

			switch ($this->method()) {
				case MethodsHttp::METHOD_GET:
				case MethodsHttp::METHOD_DELETE:
					{
						return [];
					}
				case MethodsHttp::METHOD_POST:
					{
						return [
							'nombre'        => 'required',
							'tipo_tarea_id' => 'required',
							'user_id'       => 'required',
						];
					}
				case MethodsHttp::METHOD_PUT:
					{
						return [
							'nombre'        => 'required',
							'tipo_tarea_id' => 'required',
						];
					}
				default:
					return null;
			}
		}

		/**
		 * Funcion que maneja los mensajes de la validacion
		 *
		 * @return array
		 */
		public function messages() {

			return [
				'required' => 'El campo :attribute es requerido.',
			];
		}

		/**
		 * Funcion que maneja los nombres alternativos de la validacion
		 *
		 * @return array
		 */
		public function attributes() {

			return [
				'nombre'        => 'Nombre de la tarea',
				'tipo_tarea_id' => 'Id del tipo de la tarea',
				'user_id'       => 'Id del usuario',
			];
		}

		/**
		 * Metodo que evita la redireccion en caso de fallo
		 *
		 * @param \Illuminate\Contracts\Validation\Validator $validator
		 */
		protected function failedValidation(Validator $validator) {

			throw new HttpResponseException(
				response()->json(
					$validator->errors(),
					CodesResponse::CODE_FORM_INVALIDATE
				)
			);
		}
	}
