<?php

	namespace Tareas\Globals;

	class Parametricas {

		/**
		 * @global int Tiempo que dura la cache en minutos
		 */
		const CACHE_TIME = 1;

		/**
		 * @global int Tipo de usuario developer
		 */
		const TIPO_USER_DEVELOPER = 1;

		/**
		 * @global int Tipo de usuario administrador
		 */
		const TIPO_USER_ADMINISTRADOR = 2;

		/**
		 * @global int Tipo de usuario regular
		 */
		const TIPO_USER_REGULAR = 3;

		/**
		 * @global int Tipo de tarea postre
		 */
		const TIPO_TAREA_EXAMEN = 1;

		/**
		 * @global int Tipo de tarea plato fuerte
		 */
		const TIPO_TAREA_PRESENTACION = 2;

		/**
		 * @global int Tipo de tarea sopa
		 */
		const TIPO_TAREA_HOGAR = 3;

	}
