<?php

	namespace Tareas\Models;

	use Illuminate\Database\Eloquent\Model;

	class TipoUser extends Model {

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'id',
			'sigla',
			'nombre',
		];

		/**
		 * Metodo que relaciona con User
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\HasMany
		 */
		public function user() {

			return $this->hasMany(User::class);
		}

	}
