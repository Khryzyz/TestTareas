<?php

	namespace Tareas\Models;

	use Illuminate\Database\Eloquent\Model;

	class TipoTarea extends Model {

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'id',
			'nombre',
		];

		/**
		 * Metodo que relaciona con Tarea
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\HasMany
		 */
		public function user() {

			return $this->hasMany(Tarea::class);
		}
	}
