<?php

	namespace Tareas\Models;

	use Illuminate\Database\Eloquent\Model;

	class Tarea extends Model {

		use SoftDeletes;

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'nombre',
			'tipo_tarea_id',
			'user_id',
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
			'deleted_at',
		];

		/**
		 *  The attributes that are considered by softDelete
		 *
		 * @var array
		 */
		protected $dates = [
			'created_at',
			'updated_at',
			'deleted_at',
		];

		/**
		 * Relaciones ***********************************************************************************************
		 */

		/**
		 * Metodo que relaciona con TipoTarea
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
		public function tipoTarea() {

			return $this->belongsTo(TipoTarea::class);
		}
	}
