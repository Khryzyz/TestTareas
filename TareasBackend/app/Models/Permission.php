<?php

	namespace Tareas\Models;

	use Illuminate\Database\Eloquent\Model;

	class Permission extends Model {

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'name',
			'guard_name',
		];

	}
