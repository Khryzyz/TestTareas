<?php

	namespace Tareas\Models;

	use Illuminate\Database\Eloquent\SoftDeletes;
	use Illuminate\Foundation\Auth\User as Authenticatable;
	use Illuminate\Notifications\Notifiable;
	use Spatie\Permission\Traits\HasRoles;
	use Tymon\JWTAuth\Contracts\JWTSubject;

	class User extends Authenticatable implements JWTSubject {

		use Notifiable;
		use SoftDeletes;
		use HasRoles;

		/**
		 * The attributes that name the guard
		 *
		 * @var string
		 */
		protected $guard_name = 'api';

		/**
		 * The attributes that are mass assignable.
		 *
		 * @var array
		 */
		protected $fillable = [
			'email',
			'password',
			'nombre',
			'apellido',
			'tipo_user_id',
		];

		/**
		 * The attributes that should be hidden for arrays.
		 *
		 * @var array
		 */
		protected $hidden = [
			'password',
			'remember_token',
			'deleted_at',
		];

		/**
		 *  The attributes that are considered by softDelete
		 *
		 * @var array
		 */
		protected $dates = [
			'created_at',
			'updated_at',
			'deleted_at',
		];

		/**
		 * Mutadores ************************************************************************************************
		 */
		/**
		 * Mutador para el atibuto de password
		 *
		 * @param $value
		 */
		public function setPasswordAttribute($value) {

			$this->attributes['password'] = bcrypt($value);
		}

		/**
		 * Relaciones ***********************************************************************************************
		 */
		/**
		 * Metodo que relaciona con TipoUser
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
		 */
		public function tipoUser() {

			return $this->belongsTo(TipoUser::class);
		}

		/**
		 * Metodo que relaciona con Curso
		 *
		 * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
		 */
		public function curso() {

			return $this->belongsToMany(Curso::class);
		}

		/**
		 * Metodos agregados para uso de JWT ************************************************************************
		 */
		/**
		 * Get the identifier that will be stored in the subject claim of the JWT.
		 *
		 * @return mixed
		 */
		public function getJWTIdentifier() {

			return $this->getKey();
		}

		/**
		 * Return a key value array, containing any custom claims to be added to the JWT.
		 *
		 * @return array
		 */
		public function getJWTCustomClaims() {

			return [];
		}
	}
