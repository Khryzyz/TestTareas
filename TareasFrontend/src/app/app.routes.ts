import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/content/home/home.component';
import {UsersComponent} from './components/content/users/users.component';
import {CursosComponent} from './components/content/cursos/cursos.component';
import {TareasComponent} from './components/content/tareas/tareas.component';
import {LoginComponent} from './components/content/login/login.component';

const APP_ROUTES: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'users', component: UsersComponent},
  {path: 'cursos', component: CursosComponent},
  {path: 'tareas', component: TareasComponent},
  {path: 'login', component: LoginComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash: true});
