import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../../../services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent {

  statusSession: any;

  /**
   * Constructor de la clase
   *
   * @param {Router} router
   * @param {LoginService} loginService
   */
  constructor(
    public router: Router,
    private loginService: LoginService
  ) {
    this.statusSession = this.loginService.statusSession;

    this.loginService.verifyStatusSession().subscribe(
      data => {
        this.statusSession = data;
      }
    );
  }

  logout() {
    this.router.navigate(['login']);
    this.loginService.logout();
  }

}
