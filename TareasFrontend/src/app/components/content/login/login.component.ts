import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {

  protected email: string = 'perez@hotmail.com';
  protected pass: string = '12345';
  protected loading: boolean = false;
  protected mssStatus: boolean = false;
  protected mss;


  /**
   * Constructor de la clase
   *
   * @param {LoginService} loginService
   * @param {Router} router
   */
  constructor(
    public loginService: LoginService,
    private router: Router) {
  }

  login() {
    this.loading = true;
    this.loginService.toLogin(this.email, this.pass).subscribe((data: any) => {
      console.log(data);

      if (data.status == 'success') {
        localStorage.setItem('token', data.data.token);
        localStorage.setItem('rol', data.rol);
        this.router.navigate(['panel']);
        this.loading = false;
        this.loginSer.logIn();
      }

    }, (error: any) => {
      console.log(error);
      if (error.error.status == 'error') {
        this.mostrarMss('Revisa tu correo electronico y contraseña');
        this.loading = false;
      } else {
        this.mostrarMss('Problemas de conexion :(!');
        this.loading = false;
      }
    });
  }

  register() {
    this.router.navigate(['register']);
  }


  mostrarMss(mss) {
    this.mssStatus = true;
    this.mss = mss;
    setTimeout(() => {
      this.mssStatus = false;
    }, 4000);
  }
}
