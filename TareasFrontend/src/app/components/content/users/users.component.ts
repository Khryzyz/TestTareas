import {Component, OnInit} from '@angular/core';
import {UsersService} from "../../../services/users.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {

  constructor (private _usersService: UsersService) {
  }

  ngOnInit () {
  }

}
