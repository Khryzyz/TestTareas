import {Injectable} from '@angular/core';
import {Globals} from '../app.globals';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private session = new Subject<boolean>();

  statusSession: boolean;

  token: string;

  /**
   * Constructor de la clase
   *
   * @param {HttpClient} http
   * @param {Globals} globals
   */
  constructor(public http: HttpClient, public globals: Globals) {

    this.token = localStorage.getItem(this.globals.keyLocalStorageTokenAuth);

    this.statusSession = !!localStorage.getItem(this.globals.keyLocalStorageTokenAuth);

  }


  toLogin(email, pass) {
    let params = {
      email: email,
      password: pass
    };
    // let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this.http.post(`${this.globals.urlApi}/login`, params);
  }

  /**
   *
   *
   * @returns {Observable<boolean>}
   */
  verifyStatusSession(): Observable<boolean> {
    return this.session.asObservable();
  }

  /**
   *
   */
  login() {
    this.statusSession = true;
    this.session.next(this.statusSession);
  }

  /**
   *
   */
  logout() {
    localStorage.removeItem(this.globals.keyLocalStorageTokenAuth);
    this.statusSession = false;
    this.session.next(this.statusSession);
  }

}
