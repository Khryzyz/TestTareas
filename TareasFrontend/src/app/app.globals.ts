import {Injectable} from '@angular/core';

@Injectable()
export class Globals {
  url: string = 'http://localhost/';
  urlApi: string = this.url + 'api';
  keyLocalStorageTokenAuth: string = 'keyLocalStorageTokenAuth';
}
