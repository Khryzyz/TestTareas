import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

//Modulos
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';

//Global Vars
import {Globals} from './app.globals';

//Rutas
import {APP_ROUTING} from './app.routes';

//Servicios
import {UsersService} from './services/users.service';

//Components
import {AppComponent} from './app.component';
//UI
import {NavbarComponent} from './components/ui/navbar/navbar.component';
import {FooterComponent} from './components/ui/footer/footer.component';
//Content
import {LoginComponent} from './components/content/login/login.component';
import {HomeComponent} from './components/content/home/home.component';
import {CursosComponent} from './components/content/cursos/cursos.component';
import {TareasComponent} from './components/content/tareas/tareas.component';
import {UsersComponent} from './components/content/users/users.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    CursosComponent,
    TareasComponent,
    UsersComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    NgbModule,
    HttpClientModule
  ],
  providers: [
    Globals,
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
