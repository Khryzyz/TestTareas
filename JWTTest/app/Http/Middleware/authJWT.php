<?php

	namespace App\Http\Middleware;

	use Closure;
	use App\Globals\CodesResponse;
	use App\Globals\KeysResponse;
	use App\Globals\MessageResponse;
	use Tymon\JWTAuth\Exceptions\JWTException;
	use Tymon\JWTAuth\Exceptions\TokenExpiredException;
	use Tymon\JWTAuth\JWTAuth;

	class authJWT {

		/**
		 * The JWT Authenticator.
		 *
		 * @var \Tymon\JWTAuth\JWTAuth
		 */
		protected $auth;

		/**
		 * Create a new BaseMiddleware instance.
		 *
		 * @param  \Tymon\JWTAuth\JWTAuth $auth
		 * @return void
		 */
		public function __construct(JWTAuth $auth) {

			$this->auth = $auth;
		}

		/**
		 * Handle an incoming request.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  \Closure $next
		 * @return mixed
		 */
		public function handle($request, Closure $next) {

			$error           = FALSE;
			$responseStatus  = NULL;
			$responseMessage = NULL;
			$responseCode    = NULL;
			try {
				if(!$this->auth->parser()->setRequest($request)->hasToken()) {
					$error           = TRUE;
					$responseStatus  = KeysResponse::STATUS_ERROR;
					$responseMessage = MessageResponse::MESSAGE_AUTH_TOKEN_NOT_FOUND;
					$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
				}
				if(!$this->auth->parseToken()->authenticate()) {
					$error           = TRUE;
					$responseStatus  = KeysResponse::STATUS_ERROR;
					$responseMessage = MessageResponse::MESSAGE_AUTH_USER_NOT_FOUND;
					$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
				}
			} catch(TokenExpiredException $exception) {
				$error           = TRUE;
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_AUTH_TOKEN_EXPIRED;
				$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
			} catch(JWTException $e) {
				$error           = TRUE;
				$responseStatus  = KeysResponse::STATUS_ERROR;
				$responseMessage = MessageResponse::MESSAGE_AUTH_TOKEN_INVALID;
				$responseCode    = CodesResponse::CODE_UNAUTHORIZED;
			}
			if($error) {
				return response()->json(
				  [
					KeysResponse::KEY_STATUS  => $responseStatus,
					KeysResponse::KEY_MESSAGE => $responseMessage,
					KeysResponse::KEY_DATA    => NULL,
				  ],
				  $responseCode
				);
			}

			return $next($request);
		}
	}
