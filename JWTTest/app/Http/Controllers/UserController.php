<?php

	namespace App\Http\Controllers;

	use App\Globals\KeysResponse;
	use App\Globals\MethodsHttp;
	use App\Globals\Utils;
	use App\User;
	use Illuminate\Http\Request;
	use Illuminate\Notifications\Notifiable;
	use Tymon\JWTAuth\Contracts\JWTSubject;

	class UserController extends Controller implements JWTSubject {

		use Notifiable;

		/**
		 * Display a listing of the resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function index() {

			try {
				$dataModel = User::all();

				return Utils::responseTransaccion(
				  $dataModel,
				  KeysResponse::STATUS_SUCCESS,
				  MethodsHttp::METHOD_GET
				);
			} catch(\Exception $e) {
				return Utils::responseTransaccion(
				  NULL,
				  KeysResponse::STATUS_ERROR,
				  MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id) {

			try {
				$dataModel = User::findorfail($id);

				return Utils::responseTransaccion(
				  $dataModel,
				  KeysResponse::STATUS_SUCCESS,
				  MethodsHttp::METHOD_GET
				);
			} catch(\Exception $e) {
				return Utils::responseTransaccion(
				  NULL,
				  KeysResponse::STATUS_ERROR,
				  MethodsHttp::METHOD_GET
				);
			}
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(Request $request) {
			//
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(Request $request, $id) {
			//
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id) {
			//
		}

		/**
		 * Metodos de la interface de JWT
		 */
		/**
		 * Get the identifier that will be stored in the subject claim of the JWT.
		 *
		 * @return mixed
		 */
		public function getJWTIdentifier() {

			return $this->getKey();
		}

		/**
		 * Return a key value array, containing any custom claims to be added to the JWT.
		 *
		 * @return array
		 */
		public function getJWTCustomClaims() {

			return [];
		}
	}
