<?php

	namespace App\Globals;

	class MessageResponse {

		/**
		 * @global String constante para la autenticacion de usuario encontrado
		 */
		const MESSAGE_AUTH_USER_FOUND = "Usuario encontrado";
		/**
		 * @global String constante para la autenticacion de usuario no encontrado
		 */
		const MESSAGE_AUTH_USER_NOT_FOUND = "Usuario no encontrado";
		/**
		 * @global String constante para la autenticacion de usuario no encontrado
		 */
		const MESSAGE_AUTH_USER_ERROR = "Error en el usuario";
		/**
		 * @global String constante para la autenticacion token invalido
		 */
		const MESSAGE_AUTH_TOKEN_EXPIRED = "Token expirado";
		/**
		 * @global String constante para la autenticacion token invalido
		 */
		const MESSAGE_AUTH_TOKEN_INVALID = "Token invalido";
		/**
		 * @global String constante para la autenticacion token no encontrado
		 */
		const MESSAGE_AUTH_TOKEN_NOT_FOUND = "Token no encontrado";
		/**
		 * @global String constante para el login con exitosa
		 */
		const MESSAGE_LOGIN_SUCCESS = "Usuario logueado con exito";
		/**
		 * @global String constante para el login con fallido
		 */
		const MESSAGE_LOGIN_FAIL = "Usuario o contraseña invalido";
		/**
		 * @global String constante para el login con exitosa
		 */
		const MESSAGE_LOGIN_ERROR = "Ocurrio un error durante la autenticación";
		/**
		 * @global String constante para el logout con exitosa
		 */
		const MESSAGE_LOGOUT_SUCCESS = "Salida de usuario con exito";
		/**
		 * @global String constante para el logout con exitosa
		 */
		const MESSAGE_LOGOUT_ERROR = "Ocurrio un error durante la salida";
		/**
		 * @global String constante para el refresh con exitosa
		 */
		const MESSAGE_REFRESH_SUCCESS = "Actualización de token exitosa";
		/**
		 * @global String constante para el refresh con exitosa
		 */
		const MESSAGE_REFRESH_ERROR = "Ocurrio un error durante la actualizacion";
		/**
		 * @global String constante para el mensaje de consulta exitosa
		 */
		const MESSAGE_QUERY_SUCCESS = "La busqueda produjo resultados";
		/**
		 * @global String constante para el mensaje de consulta vacia
		 */
		const MESSAGE_QUERY_EMPTY = "La busqueda NO produjo resultados";
		/**
		 * @global String constante para el mensaje de consulta error
		 */
		const MESSAGE_QUERY_ERROR = "La busqueda presenta errores";
		/**
		 * @global String constante para el mensaje de actualizacion exitoso
		 */
		const MESSAGE_UPDATE_SUCCESS = "La actualizacion fue existosa";
		/**
		 * @global String constante para el mensaje de actualizacion con error
		 */
		const MESSAGE_UPDATE_ERROR = "No fue posible realizar la actualizacion";
		/**
		 * @global String constante para el mensaje de creacion exitosa
		 */
		const MESSAGE_CREATE_SUCCESS = "El registro fue creado";
		/**
		 * @global String constante para el mensaje de crear con error
		 */
		const MESSAGE_CREATE_ERROR = "No fue posible crear el registro";
		/**
		 * @global String constante para el mensaje de creacion exitosa
		 */
		const MESSAGE_DELETE_SUCCESS = "El registro fue eliminado";
		/**
		 * @global String constante para el mensaje de crear con error
		 */
		const MESSAGE_DELETE_ERROR = "No fue posible eliminar el registro";
	}
